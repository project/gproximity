<?php

/**
 * @file
 * Provide gProximity display type for Views
 */

/**
 * Implementation of hook_views_plugins
 */
function gproximity_views_plugins() {
  $path = drupal_get_path('module', 'views') . '/js';
  return array(
    'display' => array(
      'gproximity' => array(
        'title' => t('gProximity'),
        'help' => t('Display a Google map with nodes for markers.'),
        'handler' => 'gproximity_plugin_display',
        'theme' => 'gproximity_view_display',
        'parent' => 'page', // so it knows to load the page plugin .inc file
        // 'path' => drupal_get_path('module', 'gproximity'),
        'uses hook menu' => TRUE,
        'use ajax' => FALSE,
        'use pager' => TRUE,
        'base' => array('node'),
        'uses options' => TRUE,
        'uses row plugin' => TRUE,
        'accept attachments' => TRUE,
        'admin' => t('gProximity'),
        // 'help topic' => 'display-gproximity'
      )
    )
  );
}
