<?php
/**
 * @file
 * Contains the feed display plugin.
 */

/**
 * The plugin that handles a gProximity Google Map.
 *
 * A gProximity display generates a Google Map with nodes as the markers.
 *
 * @ingroup views_display_plugins
 */
class gproximity_plugin_display extends views_plugin_display_page {
  function get_style_type() { return 'gproximity'; }

  function preview() {
    return '<pre>' . check_plain($this->view->render()) . '</pre>';
  }

  /**
   * Instead of going through the standard views_view.tpl.php, delegate this
   * to the style handler.
   */
  function render() {
    return $this->view->style_plugin->render($this->view->result);
  }

  function option_definition() {
    $options = parent::option_definition();

    // Page settings
    $options['search'] = array('default' => 1);
    $options['search_settings'] = array(
      'contains' => array(
        'title' => array('default' => t('Search')),
        'description' => array('default' => t('Enter an address to search for.')),
        'size' => array('default' => 35),
        'button' => array('default' => 1)
      )
    );
    if (empty($search_settings)) {
      $search_settings = array(
        'title' => t('Search'),
        'description' => t('Enter an address to search for.'),
        'size' => 35,
        'button' => 1
      );
    }
    $options['results'] = array('default' => 1);
    $options['results_settings'] = array(
      'contains' => array(
        'title' => array('default' => t('Results')),
        'distance' => array('default' => 50)
      )
    );
    $api_key = keys_get_key('gproximity', array('kid', 'service', 'rule', 'api_key'), TRUE);
    $options['api_key'] = array(
      'contains' => array(
        'kid' => array('default' => $api_key['kid']),
        'service' => array('default' => $api_key['service']),
        'rule' => array('default' => $api_key['rule']),
        'api_key' => array('default' => $api_key['api_key'])
      )
    );

    // Map
    $options['dimensions'] = array(
      'contains' => array(
        'width' => array('default' => '500px'),
        'height' => array('default' => '450px')
      )
    );
    $options['zoom_levels'] = array(
      'contains' => array(
        'initial' => array('default' => 5),
        'search' => array('default' => 8)
      )
    );
    $options['map_center'] = array(
      'contains' => array(
        'latitude' => array('default' => 40),
        'longitude' => array('default' => 0)
      )
    );
    $options['controls'] = array(
      'contains' => array(
        'scale' => array('default' => 0),
        'type' => array('default' => 0),
        'zoom' => array('default' => 2),
        'overview' => array(
          'contains' => array(
            'visible' => array('default' => 'visible'),
            'initial' => array('default' => 0)
          )
        ),
        'doubleclick' => array('default' => 0),
        'scrollzoom' => array('default' => 0),
        'dragging' => array('default' => 1),
        'continuouszoom' => array('default' => 0)
      )
    );

    // Markers
    $options['icon'] = array('default' => '');
    $options['zoom_range'] = array(
      'contains' => array(
        'out' => array('default' => 0),
        'in' => array('default' => 'max')
      )
    );
    $options['geo_fields'] = array(
      'latitude' => array('default' => ''),
      'longitude' => array('default' => '')
    );
    $options['info_window'] = array('default' => '');
    $options['clusters'] = array(
      'default' => array()
    );

    return $options;
  }

  function options_summary(&$categories, &$options) {
    // It is very important to call the parent function here:
    parent::options_summary($categories, $options);

    $categories['map'] = array(
      'title' => t('Map'),
    );
    $categories['markers'] = array(
      'title' => t('Markers'),
    );

    // Page settings
    $search = $this->get_option('search');
    $options['search'] = array(
      'category' => 'page',
      'title' => t('Search'),
      'value' => ($search) ? t('On') : t('Off'),
      'links' => array('search_settings' => t('Change the maps search settings'))
    );
    $results = $this->get_option('results');
    $options['results'] = array(
      'category' => 'page',
      'title' => t('Results'),
      'value' => ($results) ? t('On') : t('Off'),
      'links' => array('results_settings' => t('Change the maps results settings'))
    );
    $api_key = $this->get_option('api_key');
    $options['api_key'] = array(
      'category' => 'page',
      'title' => t('Google Map API Key'),
      'value' => (!empty($api_key)) ? $api_key['api_key'] .' @ '. $api_key['rule'] : t('None')
    );

    // Map
    $dimensions = $this->get_option('dimensions');
    $options['dimensions'] = array(
      'category' => 'map',
      'title' => t('Dimensions'),
      'value' => "'". $dimensions['width'] ."' x '". $dimensions['height'] ."'"
    );
    $zoom_levels = $this->get_option('zoom_levels');
    $options['zoom_levels'] = array(
      'category' => 'map',
      'title' => t('Zoom level'),
      'value' => 'Initial('. $zoom_levels['initial'] .'), Search('. $zoom_levels['search'] .')'
    );
    $map_center = $this->get_option('map_center');
    $options['map_center'] = array(
      'category' => 'map',
      'title' => t('Map center'),
      'value' => $map_center['latitude'] .', '. $map_center['longitude']
    );
    $controls = $this->get_option('controls');
    // Build list of enabled controls
    $enabled_controls = array();
    foreach ($controls as $control => $value) {
      if ($value) {
        switch ($control) {
          case 'overview':
            if (in_array('visible', array_filter($value))) {
              $enabled_controls[] = ucwords($control);
            }
            break;
          default:
            $enabled_controls[] = ucwords($control);
            break;
        }
      }
    }
    if (empty($enabled_controls)) {
      $enabled_controls[] = t('None');
    }
    $options['controls'] = array(
      'category' => 'map',
      'title' => t('Controls'),
      'value' => implode(', ', $enabled_controls)
    );

    // Markers
    $icon = $this->get_option('icon');
    $options['icon'] = array(
      'category' => 'markers',
      'title' => t('Icon'),
      'value' => '<img src="http://maps.google.com/mapfiles/ms/micons/blue.png" />'
    );
    $zoom_range = $this->get_option('zoom_range');
    $options['zoom_range'] = array(
      'category' => 'markers',
      'title' => t('Zoom range'),
      'value' => $zoom_range['out'] .' - '. (($zoom_range['in'] == 'max') ? t('No limit') : $zoom_range['in'])
    );
    $geo_fields = $this->get_option('geo_fields');
    $fields = $this->display->handler->get_handlers('field');
    $options['geo_fields'] = array(
      'category' => 'markers',
      'title' => t('Geocode fields'),
      'value' => ($geo_fields['latitude'] && $geo_fields['longitude']) ? $fields[$geo_fields['latitude']]->ui_name() .', '. $fields[$geo_fields['longitude']]->ui_name() : t('None')
    );
    $info_window = $this->get_option('info_window');
    $options['info_window'] = array(
      'category' => 'markers',
      'title' => t('Info'),
      'value' => ($info_window) ? t('Set') : t('None')
    );
    $clusters = $this->get_option('clusters');
    $options['clusters'] = array(
      'category' => 'markers',
      'title' => t('Clusters'),
      'value' => t('FixMe')
    );
  }

  /**
   * Provide the default form for setting options.
   */
  function options_form(&$form, &$form_state) {
    // It is very important to call the parent function here.
    parent::options_form($form, $form_state);

    switch ($form_state['section']) {
      case 'search':
        $form['#title'] .= t('Search field for the map');

        $search = gproximity_get_option('search', $this->get_option('search'));

        $form['enabled'] = array(
          '#type' => 'checkbox',
          '#title' => t('Enable search field'),
          '#description' => t('Provide a search field to find markers on the map.'),
          '#default_value' => $search
        );
        break;
      case 'search_settings':
        $form['#title'] .= t('The search field settings');

        $search_settings = $this->get_option('search_settings');

        $form['title'] = array(
          '#type' => 'textfield',
          '#title' => t('Name'),
          '#description' => t('Specify the title for the search field.'),
          '#default_value' => $search_settings['title']
        );
        $form['description'] = array(
          '#type' => 'textarea',
          '#title' => t('Description'),
          '#description' => t('Help text to be displayed under the search field.'),
          '#default_value' => $search_settings['description']
        );
        $form['size'] = array(
          '#type' => 'textfield',
          '#title' => t('Size'),
          '#description' => t('Size of the search field.'),
          '#default_value' => $search_settings['size'],
          '#size' => 10
        );
        $form['button'] = array(
          '#type' => 'checkbox',
          '#title' => t('Show button'),
          '#description' => t('Show the submit button.'),
          '#default_value' => $search_settings['button']
        );
        break;
      case 'results':
        $form['#title'] .= t('Results from searching and navigating the map');

        $results = gproximity_get_option('results', $this->get_option('results'));

        $form['enabled'] = array(
          '#type' => 'checkbox',
          '#title' => t('Enable results'),
          '#description' => t('Displays the results of the search and/or the results from navigating the map.'),
          '#default_value' => $results
        );
        break;
      case 'results_settings':
        $form['#title'] .= t('The results settings');

        $results_settings = $this->get_option('results_settings');

        $form['title'] = array(
          '#type' => 'textfield',
          '#title' => t('Title'),
          '#description' => t('Title for the results.'),
          '#default_value' => $results_settings['title']
        );
        $form['distance'] = array(
          '#type' => 'textfield',
          '#title' => t('Distance limit'),
          '#description' => t('The max distance (in miles) to display results for. If there are no results to display, the closest result will be returned.'),
          '#default_value' => $results_settings['distance'],
          '#required' => TRUE,
          '#size' => 5
        );
        break;
      case 'api_key':
        $api_key = $this->get_option('api_key');
        $form['keys'] = keys_settings_form($api_key);
        break;
      case 'dimensions':
        $form['#title'] .= t('The map dimensions');

        $dimensions = $this->get_option('dimensions');

        $form['width'] = array(
          '#type' => 'textfield',
          '#title' => t('Width'),
          '#default_value' => $dimensions['width'],
          '#required' => TRUE,
          '#size' => 8,
          '#maxlength' => 8
        );
        $form['height'] = array(
          '#type' => 'textfield',
          '#title' => t('Height'),
          '#default_value' => $dimensions['height'],
          '#required' => TRUE,
          '#size' => 8,
          '#maxlength' => 8
        );
        break;
      case 'zoom_levels':
        $form['#title'] .= t('The map initial and search zoom levels');

        $zoom_options = array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17);
        $zoom_levels = $this->get_option('zoom_levels');

        $form['initial'] = array(
          '#type' => 'select',
          '#title' => t('Initial zoom level'),
          '#description' => t('Initial zoom level when the map is loaded.'),
          '#options' => $zoom_options,
          '#default_value' => $zoom_levels['initial'],
          '#required' => TRUE
        );
        $form['search'] = array(
          '#type' => 'select',
          '#title' => t('Search zoom level'),
          '#description' => t('Zoom level when search is performed.'),
          '#options' => $zoom_options,
          '#default_value' => $zoom_levels['search'],
          '#required' => TRUE
        );
        break;
      case 'map_center':
        $form['#title'] .= t('The initial map center');

        $map_center = $this->get_option('map_center');

        $form['latitude'] = array(
          '#type' => 'textfield',
          '#title' => t('Latitude'),
          '#default_value' => $map_center['latitude'],
          '#required' => TRUE,
          '#size' => 40,
          '#maxlength' => 40
        );
        $form['longitude'] = array(
          '#type' => 'textfield',
          '#title' => t('Longitude'),
          '#default_value' => $map_center['longitude'],
          '#required' => TRUE,
          '#size' => 40,
          '#maxlength' => 40
        );
        break;
      case 'controls':
        $form['#title'] .= t('The map controls');

        $controls = $this->get_option('controls');

        $form['scale'] = array(
          '#type' => 'checkbox',
          '#title' => t('Scale'),
          '#description' => t('Display the map scale.'),
          '#default_value' => $controls['scale']
        );
        $form['type'] = array(
          '#type' => 'checkbox',
          '#title' => t('Map types'),
          '#description' => t('Display buttons to switch between map types.'),
          '#default_value' => $controls['type']
        );
        $form['zoom'] = array(
          '#type' => 'select',
          '#title' => t('Zoom'),
          '#description' => t('Display buttons to zoom in and out. Large includes a zoom slider.'),
          '#options' => array(t('None'), t('Small - Zoom in and out'), t('Small - Zoom in and out; pan in four directions'), t('Large - Zoom in and out via slider; pan in four directions;')),
          '#default_value' => $controls['zoom']
        );
        $form['overview'] = array(
          '#type' => 'checkboxes',
          '#title' => t('Overview'),
          '#description' => t('Display a collapsible overview mini-map in the corner of the map for reference location and navigation (through dragging).'),
          '#options' => array('visible' => t('Display map overview'), 'initial' => t('Overview, if displayed, is shown on map load')),
          '#default_value' => $controls['overview']
        );
        $form['doubleclick'] = array(
          '#type' => 'checkbox',
          '#title' => t('Double-click zoom'),
          '#description' => t('Enables double click zooming.'),
          '#default_value' => $controls['doubleclick']
        );
        $form['scrollzoom'] = array(
          '#type' => 'checkbox',
          '#title' => t('Scroll-wheel zoom'),
          '#description' => t("Enables zooming using a mouse's scroll wheel."),
          '#default_value' => $controls['scrollzoom']
        );
        $form['dragging'] = array(
          '#type' => 'checkbox',
          '#title' => t('Dragging'),
          '#description' => t('Enables the ability to drag the map.'),
          '#default_value' => $controls['dragging']
        );
        $form['continuouszoom'] = array(
          '#type' => 'checkbox',
          '#title' => t('Continuous zoom'),
          '#description' => t('Enables continuous smooth zooming for select browsers.'),
          '#default_value' => $controls['continuouszoom']
        );
        break;
      case 'icon':
        $form['#title'] .= t('Marker icon');

        $icon = $this->get_option('icon');

        $form['icon'] = array(
          '#type' => 'select',
          '#title' => t('Icon'),
          '#description' => t('Select an icon to be used for the markers.'),
          '#options' => array(),
          '#default_value' => $icon,
          '#required' => TRUE
        );
        break;
      case 'zoom_range':
        $form['#title'] .= t('Markers zoom range');

        $zoom_options = array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17);
        $zoom_range = $this->get_option('zoom_range');

        $form['out'] = array(
          '#type' => 'select',
          '#title' => t('Zoom out level'),
          '#description' => t('Specify a low number for zooming out.'),
          '#options' => $zoom_options,
          '#default_value' => $zoom_range['out'],
          '#required' => TRUE
        );
        $form['in'] = array(
          '#type' => 'select',
          '#title' => t('Zoom in level'),
          '#description' => t('Specify a high number for zooming in.'),
          '#options' => $zoom_options + array('max' => t('No limit')),
          '#default_value' => $zoom_range['in'],
          '#required' => TRUE
        );
        break;
      case 'geo_fields':
        $form['#title'] .= t('Latitude and longitude fields');

        $geo_fields = $this->get_option('geo_fields');
        $options = array();
        foreach ($this->display->handler->get_handlers('field') as $field => $handler) {
          if ($label = $handler->label()) {
            $options[$field] = $label;
          } else {
            $options[$field] = $handler->ui_name();
          }
        }

        $form['latitude'] = array(
          '#type' => 'select',
          '#title' => t('Latitude field'),
          '#description' => t('Select the field that holds the latitude value.'),
          '#options' => $options,
          '#default_value' => $geo_fields['latitude'],
          '#required' => TRUE
        );
        $form['longitude'] = array(
          '#type' => 'select',
          '#title' => t('Longitude field'),
          '#description' => t('Select the field that holds the longitude value.'),
          '#options' => $options,
          '#default_value' => $geo_fields['longitude'],
          '#required' => TRUE
        );
        break;
      case 'info_window':
        $form['#title'] .= t('Marker info window');

        $info = $this->get_option('info_window');

        $form['info_window'] = array(
          '#type' => 'textarea',
          '#title' => t('Info window'),
          '#description' => t('Provide information about the node to show in the results and the markers info window. Tokens can be used.'),
          '#default_value' => $info,
          '#rows' => 5
        );
        break;
      case 'clusters':
        $form['#title'] .= t('Clusters');

        $clusters = $this->get_option('clusters');

        
        break;
    }
  }

  function options_validate($form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_validate($form, $form_state);

    switch ($form_state['section']) {
      case 'api_key':
        _keys_settings_form_validate($form, $form_state);
        break;
      case 'search_settings':
        if (!is_numeric($form_state['values']['size'])) {
          form_error($form['size'], t('Size has to be numeric.'));
        }
        break;
      case 'map_center':
        if (!is_numeric($form_state['values']['latitude'])) {
          form_error($form['latitude'], t('Latitude has to be numeric.'));
        }
        if (!is_numeric($form_state['values']['longitude'])) {
          form_error($form['longitude'], t('Longitude has to be numeric.'));
        }
        break;
      case 'zoom_range':
        if ($form_state['values']['out'] > $form_state['values']['in']) {
          form_error($form['out'], t('Zoom out level cannot be greater than the zoom in level.'));
        }
        break;
      case 'geo_fields':
        if ($form_state['values']['latitude'] == $form_state['values']['longitude']) {
          form_error($form['longitude'], t('Latitude and longitude cannot use the same field.'));
        }
        break;
    }
  }

  /**
   * Perform any necessary changes to the form values prior to storage.
   * There is no need for this function to actually store the data.
   */
  function options_submit($form, &$form_state) {
    // It is very important to call the parent function here:
    parent::options_submit($form, $form_state);

    $values = $form_state['values'];
    switch ($form_state['section']) {
      case 'api_key':
        $api_key = array(
          'kid' => $values['kid'],
          'service' => $values['service'],
          'rule' => $values['rule'],
          'api_key' => $values['api_key']
        );
        $this->set_option('api_key', $api_key);
        _keys_settings_form_submit($form, $form_state);
        break;
      case 'search':
        $this->set_option('search', $values['search']);
        break;
      case 'search_settings':
        $search_settings = array(
          'title' => $values['title'],
          'description' => $values['description'],
          'size' => $values['size'],
          'button' => $values['button']
        );
        $this->set_option('search_settings', $search_settings);
        break;
      case 'results':
        $this->set_option('results', $values['results']);
        break;
      case 'results_settings':
        $results_settings = array(
          'title' => $values['title'],
          'description' => $values['description']
        );
        $this->set_option('results_settings', $form_state['values']['results']);
        break;
      case 'dimensions':
        $dimensions = array(
          'width' => $values['width'],
          'height' => $values['height']
        );
        $this->set_option('dimensions', $dimensions);
        break;
      case 'zoom_levels':
        $zoom_levels = array(
          'initial' => $values['initial'],
          'search' => $values['search']
        );
        $this->set_option('zoom_levels', $zoom_levels);
        break;
      case 'map_center':
        $map_center = array(
          'latitude' => $values['latitude'],
          'longitude' => $values['longitude']
        );
        $this->set_option('map_center', $map_center);
        break;
      case 'controls':
        $controls = array(
          'scale' => $values['scale'],
          'type' => $values['type'],
          'zoom' => $values['zoom'],
          'overview' => $values['overview'],
          'doubleclick' => $values['doubleclick'],
          'scrollzoom' => $values['scrollzoom'],
          'dragging' => $values['dragging'],
          'continuouszoom' => $values['continuouszoom']
        );
        $this->set_option('controls', $controls);
        break;
      case 'icon':
        $this->set_option('icon', $values['icon']);
        break;
      case 'zoom_range':
        $zoom_range = array(
          'out' => $values['out'],
          'in' => $values['in']
        );
        $this->set_option('zoom_range', $zoom_range);
        break;
      case 'geo_fields':
        $geo_fields = array(
          'latitude' => $values['latitude'],
          'longitude' => $values['longitude']
        );
        $this->set_option('geo_fields', $geo_fields);
        break;
      case 'info_window':
        $this->set_option('info_window', $values['info_window']);
        break;
      case 'clusters':
        $form['#title'] .= t('Clusters');

        $clusters = $this->get_option('clusters');


        break;
    }
  }

  function validate() {
    $errors = parent::validate();

    $api_key = $this->get_option('api_key');
    if (!$api_key['api_key']) {
      $errors[] = t('Display @display uses a Google API Key but this is undefined.', array('@display' => $this->display->display_title));
    }

    $map_center = $this->get_option('map_center');
    if ($map_center['latitude'] == '' || $map_center['longitude'] == '') {
      $errors[] = t('Display @display uses latitude and longitude to set the initial map center point but at least one of these are undefined.', array('@display' => $this->display->display_title));
    }

    $geo_fields = $this->get_option('geo_fields');
    if (!$geo_fields['latitude'] || !$geo_fields['longitude']) {
      $errors[] = t('Display @display uses geocode fields but these are undefined.', array('@display' => $this->display->display_title));
    }

    return $errors;
  }
}
